// Dependencies
const Discord         = require('discord.js');
const fs              = require('fs');
const request         = require('request');
const getYouTubeID    = require('get-youtube-id');
const {google}        = require('googleapis');


// 
const paths           = JSON.parse(fs.readFileSync('./res/config/vars/filePaths.json', 'utf-8'));
const auth            = JSON.parse(fs.readFileSync(paths.auth, 'utf-8'));
const responses       = JSON.parse(fs.readFileSync(paths.responses,'utf-8'));

// Exports
module.exports = {

    addToQueue: function (queue,strID) {
        if(this.isYoutube(strID)) {
            queue.push(getYoutubeID(strID));
        }
        else {
            queue.push(strID);
        }
        return queue;
    },

    convDuration: function(duration) {
        let hours   = 0;
        let minutes = 0;
        let seconds = 0;
        while(duration >= 60) {
            if(duration >= 3600) {
                hours++;
                duration -= 3600;
            }
            else if(duration >= 60) {
                minutes++;
                duration -= 60;
            }
        }
        seconds = duration;
        
        if(hours.toString().length != 2) 
            hours   = '0' + hours;
        if(minutes.toString().length != 2)
            minutes = '0' + minutes;
        if(seconds.toString().length != 2)
            seconds = '0' + seconds;

        return hours + ':' + minutes + ':' + seconds; 
    },

    isYoutube: function (str) {
        return str.toLowerCase().indexOf("youtube.com") > -1;
    },

    getID: function (str, message, cb) {
        if (this.isYoutube(str)) {
            cb(getYouTubeID(str));
        } 
        else {
            this.searchVideo(str, message, function(id) {
                cb(id);
            });
        }
    },

    getQueue: function  (queueNames,queueLengths,message) {
        if(queueNames.length == 0) {
            message.channel.send(responses.pQEmpty);
        }
        else {
            let content = responses.playQ1 + "\n";  
            let numbers = 1;
            for (let i = 0; i<queueNames.length; i++) {
                let temp = "```";
                if(i == 0)
                    temp += "Now Playing - " + queueNames[i];
                else {
                    temp += numbers + " - " + queueNames[i];
                    numbers++;
                }
                    temp += "\nDuration - " + this.convDuration(queueLengths[i]) + "```";
                if((content + temp).length <= 2000 - 6) {
                    content += temp;
                } 
                else {
                    message.channel.send(content);
                }
            }
            message.channel.send(content);
        }
    },

    searchVideo: function (query, message, callback) {
        request("https://www.googleapis.com/youtube/v3/search?part=id&type=video&q=" + encodeURIComponent(query) + "&key=" + auth.youtube_data_key, function(error, response, body) {
                let json = JSON.parse(body);
                if(!json.items[0]) 
                    message.channel.send(responses.playError3);
                else 
                    callback(json.items[0].id.videoId);
        });
    }

} // exports end