// Dependencies
const in_array     = require('in_array');
const fs 	       = require('fs');
let now            = require('date-and-time');

//
let paths  = JSON.parse(fs.readFileSync('./res/config/vars/filePaths.json', 'utf-8'));
const auth = JSON.parse(fs.readFileSync(paths.auth,'utf-8'));

// 

// Exports
module.exports  = {

	checkRoles: function (message, rolesArray) {
		for(let i=0; i<roleArray.length; i++) {
			if(message.member.roles.find("name",rolesArray[i])) {
				return true;
			}
		}
		// only return false if loop never matches roles
		return false;
	},

	loadConfig: function (client) {
		let configs = {};
		
		client.guilds.forEach(function(guild) {
			const serverID = guild.id;
			if(!configs[serverID]) {
	        	configs[serverID] = {
	        		prefix : null,
	        		ownerID: null,
	        		aRoles : [] 
	        	};
	        }
			const filePath = "./" + paths.guildPath + "guild[" + serverID + "]/";
			const fileName = "config.json";
			
			if(!fs.existsSync(filePath+fileName)) { 
				fs.mkdirSync(filePath);
				const defs      = JSON.parse(fs.readFileSync(paths.guildDefs, 'utf-8'));
				const prefix    = defs.prefix;
				const ownerID   = guild.ownerID;
				const aRoles    = defs.aRoles;
				const content   = {
					'prefix'   : prefix,
					'ownerID'  : ownerID,
					'aRoles'   : aRoles
				};

				setTimeout(function() {
					fs.writeFile(filePath + fileName, JSON.stringify(content), function(error) {
						if(error) throw error; //this.logError(null, null, null, error);
						 configs[serverID].prefix  = content.prefix;
						 configs[serverID].ownerID = content.ownerID;
						 configs[serverID].aRoles  = content.aRoles;
					});
				}, 500);
			} // write config
			else {
				const content = JSON.parse(fs.readFileSync(filePath + fileName, 'utf-8'));
				configs[serverID].prefix  = content.prefix;
			 	configs[serverID].ownerID = content.ownerID;
			 	configs[serverID].aRoles  = content.aRoles;
			}
		});
		return configs;
	},

	logger: function (filePath, fileName, content) {
		if(!fs.existsSync(filePath + fileName)) {
			fs.writeFile(filePath + fileName, content + "\r\n", function(error) {
				if(error) throw error; //this.logError(null, null, null, error);
				console.log(content);
			});
		}
		else {
			fs.appendFile(filePath + fileName, content + "\r\n", function(error) {
				if(error) throw error; //this.logError(null, null, null, error);
				console.log(content);
			});
		}
	},

	logActivity: function (guildID, activity, args) { 
		// logs bot activity
		const currentDate = this.Now('date');
		const currentTime = this.Now('time');
		const filePath    = paths.logActivity;
		const fileName    = "log[" + currentDate + "].txt";
		const content     = currentTime + ": " +
							"guild[" + guildID + "] " +
							"activity[" + activity     + "] " +
							"args[" + args + "]";
		this.logger(filePath,fileName,content);
	},

	logCommand: function(username,command,args) { 
		// logs user commands
		const currentDate = this.Now('date');
		const currentTime = this.Now('time');
		const filePath    = paths.logCommand;
		const fileName    = "log[" + currentDate + "].txt";
		const content     = currentTime + ": " +
							"user[" + username    + "] " +
							"command[" + command     + "] " +
							"args[" + args + "]";
		this.logger(filePath,fileName,content);
	},

	logError: function(guildID, command, args, error) { 
		// logs bot activity
		const currentDate = this.Now('date');
		const currentTime = this.Now('time');
		const filePath    = paths.logError;
		const fileName    = "log[" + currentDate + "].txt";
		const content     = currentTime + ": " +
							"guild[" + guildID + "] " +
							"command[" + command     + "] " +
							"args[" + args + "] " +
							"error[" + error + "]";
		this.logger(filePath,fileName,content);
	},

	globalSend: function(client, channelName, message) {
		client.guilds.forEach(function(guild) {
			 guild.channels.forEach(function(channel) {
			 	if(channel.name == channelName) {
			 		channel.send(message);
			 	}
			 });
		});
	},

	Now: function(args) {
		if(args == "date")
			return now.format(new Date(), 'YYYY-MM-DD').toString();
		else 
			return now.format(new Date(), 'HH:mm:ss').toString();
	},

	Say: function(guild, channelName, message) {
		guild.channels.forEach(function(channel) {
		 	if(channel.name == channelName) {
		 		channel.send(message);
		 	}
		 });
	}

}