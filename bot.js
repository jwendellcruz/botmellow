// Dependencies
const Discord        = require('discord.js');
const ytdl           = require('ytdl-core');
const request        = require('request');
const fs		     = require('fs');
const fetchVideoInfo = require('youtube-info');
const in_array       = require('in_array');

// Discord Client
const botmellow      = new Discord.Client();


// Vars
const paths          = JSON.parse(fs.readFileSync("./res/config/vars/filepaths.json",'utf-8'))
const auth           = JSON.parse(fs.readFileSync(paths.auth, 'utf-8'));
const responses      = JSON.parse(fs.readFileSync(paths.responses,'utf-8'));
const logs           = JSON.parse(fs.readFileSync(paths.logs,'utf-8'));
const guildDefs      = JSON.parse(fs.readFileSync(paths.guildDefs,'utf-8'));

// Modules
const genModule      = require(paths.genModule);
const musicModule    = require(paths.musicModule);

// 
let guilds = {};
let config = {};

// Login Bot Client
botmellow.login(auth.discord_token);

// Event Handlers
botmellow.on('message', function(message) {

	const member  = message.member;
	let   text    = message.content.toLowerCase();

    if(!text.endsWith(' ')) {
    	text = text + ' ';
    }

    if(message.author.id !== auth.bot_id) {

    	
	    if(message.guild !== null && text.startsWith(config[message.guild.id].prefix)) {

	    	if(!guilds[message.guild.id]) {
	        	guilds[message.guild.id] = {
	        		queue        : [],
					queueNames   : [],
					queueLengths : [],
					isPlaying    : false,
					dispatcher   : null,
					voiceChannel : null,
					skipReq      : 0,
					skippers     : []
	        	};
	        }
	    	
	    	const prefix  = config[message.guild.id].prefix;
	        const command = text.substring(prefix.length, text.indexOf(' ')).toLowerCase();
			const args    = message.content.split(' ').slice(prefix.length).join(' ');

	        genModule.logCommand(message.author.username, command, args);

	        switch(command) {

				case "":
				    message.channel.send(responses.blank);
				break;

				case "cc":
					if(args.length == 0 || args == " ") {
						message.channel.send(responses.ccError1);
					}
					else {
						message.channel.startTyping();
						let count = parseInt(args);
						if(count < 100) {

							message.channel.bulkDelete(count,true)
							.catch(function(error) {
								if(error == "DiscordAPIError: Missing Permissions")
									message.channel.send(responses.ccError2);
								if(error) genModule.logError(message.guild.id, command, args, error);
							});
						}
						while(count >= 100) {

							message.channel.bulkDelete(100,true)
							.catch(function(error) {
								if(error) genModule.logError(message.guild.id, command, args, error);
							});
							count -= 100;
						}
					}
					setTimeout(function() {
						message.channel.stopTyping()
					}, 100);
					;
					
				break;

				case "info":
				case "i":
				    message.channel.send(responses.info);
				break;

				case 'help':
				case 'h':
				    message.channel.send(responses.help);
				break;  

				case "kys":

					if(message.author.id == auth.dev_id) {
						genModule.logActivity(message.guild.id, logs.destroy1, logs.destroy2);

						message.delete()
						.then(function() { // send going offline messages to all general channels
							genModule.globalSend(botmellow, "general", responses.destroy);
							botmellow.destroy()
							.catch(function(error) {
								genModule.logError(message.guild.id, command, null, error);
							});
						}) 
						.catch(function(error) {
							genModule.logError(message.guild.id, command, null, error);
						});
					} // args match
					else {
						genModule.logActivity(message.guild.id, logs.dAttempt, null);
						message.channel.send(responses.dAttempt);
					}
				break;

				case 'play':
				case 'p':
					if(args == " " || args.length <= 0) {
						message.channel.send(responses.playError1);
					}
					else {
						if(guilds[message.guild.id].queue.length > 0 || guilds[message.guild.id].isPlaying) {
							// Searching
							message.channel.send(responses.pSearch + "**" + args + "**");
							genModule.logActivity(message.guild.id, logs.playSearch, args);
							// 
							musicModule.getID(args, message, function(id) {
								guilds[message.guild.id].queue = musicModule.addToQueue(
									guilds[message.guild.id].queue,
									id
								);
								// Fetch
								fetchVideoInfo(id, function(err, videoInfo) {
									if(err) throw new Error(err);
									message.channel.send(responses.playAdd1 + videoInfo.title + responses.playAdd2);
									guilds[message.guild.id].queueNames.push(videoInfo.title);
									guilds[message.guild.id].queueLengths.push(videoInfo.duration);
								})
							});
						}
						else {
							if(message.member.voiceChannel !== undefined) {
								musicModule.getID(args, message, function(id) {
										isPlaying = true;
										// Searching
										message.channel.send(responses.pSearch + "**" + args + "**");
										genModule.logActivity(message.guild.id, logs.playSearch, args);

										// PlayMusic
										playMusic(id, message);
										// FetchInfo
										fetchVideoInfo(id, function(error, videoInfo) {
											if(error) genModule.logError(message.guild.id,command,args,error);
											message.channel.send(
							    				responses.playLine1 + videoInfo.title + 
							    				responses.playLine2 + musicModule.convDuration(videoInfo.duration
							    			));
							    			genModule.logActivity(message.guild.id, logs.playMusic, videoInfo.title);
							    			// Push video info
											guilds[message.guild.id].queue.push(videoInfo.videoID);
											guilds[message.guild.id].queueNames.push(videoInfo.title);
											guilds[message.guild.id].queueLengths.push(videoInfo.duration);
										});
								});
							} // join and play music if user is on a voice channel
							else {
								message.channel.send(responses.playError2);
							}
						}
					}
				break; // play

				case 'playlist':
					//add or remove playlists
					// play playlists
				break; // playlist

				case 'pqueue':
				case 'pq':
						musicModule.getQueue(
							guilds[message.guild.id].queueNames,
							guilds[message.guild.id].queueLengths,
							message
			           );
				break; // play queue

				case 'playskip':
				case 'pskip':
				case 'ps':
					if(guilds[message.guild.id].queue.length > 0) {
						switch(args) {
							case "vote":
							case "v":
								if(guilds[message.guild.id].skippers.indexOf(message.author.id) === -1) {
									guilds[message.guild.id].skippers.push(message.author.id);
									guilds[message.guild.id].skipReq++;
									if(guilds[message.guild.id].skipReq >= 
									   Math.ceil((guilds[message.guild.id].voiceChannel.members.size - 1) / 2)) {
									   	genModule.logActivity(message.guild.id, logs.playSkip, guilds[message.guild.id].queueNames[0]);
										skipSong(message);
										message.reply(responses.playSkip3);
									}
									else {
										message.reply(responses.playSkip1 + 
													  (Math.cell((voiceChannel.members.size - 1) / 2) - skipReq) + 
													  " out of " +
													  Math.cell((voiceChannel.members.size - 1) / 2) +
													  responses.playSkip2
										);
									}
								}
								else 
									message.channel.send(responses.playSError3);
							break;

							case "f":
							case "force":
								if(message.member.roles.find("name","Crown") || message.member.roles.find("name","Counsil")) {
									genModule.logActivity(message.guild.id, logs.playSkip, guilds[message.guild.id].queueNames[0]);
									skipSong(message);
									message.reply(responses.playSForce1);
								}
								else 
									message.reply(responses.playSFError1);
							break;

							default:
								message.channel.send(responses.playSError2);
							break;
						} // skip args switch
					} // 
					else 
						message.channel.send(responses.playSError1);
				break; // play skip

				case "say":
				case "s":
					const userID  = message.author.id;
					const channel = args.substring(0, args.indexOf(' '));
					const text    = args.substring(args.indexOf(' ') + 1, args.length);

					if(userID === auth.dev_id || userID === config.owner_id || 
						genModule.checkRoles(message,config.aRoles)) {
						genModule.Say(message.guild, channel, text);
						genModule.logActivity(message.guild, logs.say, args);
					}
					else {
						message.channel.send(responses.sayError1);
					}
				break;

				default:
				    message.channel.send(responses.default);
				break; // default

			} // bot commands

	 	} // prefix

	 	else if(message.guild === null) {
	 		const command = text.substring(0, text.indexOf(' ')).toLowerCase();
	 		const args    = message.content.substring(command.length + 1, text.length);
	 		const length  = text.length;

		    genModule.logCommand(message.author.username, "DM " + command, args);

	 		switch(command) {	

	 			case "setactivity":
	 			case "setact":
	 			case "sa":
	 				if(message.author.id == auth.dev_id) {
	 					if(args == " " || args.length  < 1) {
		 					message.channel.send(responses.saError1);
		 				}
		 				else {
		 					let setArgs = args.split(" ");
		 					let setType = null;

		 					if(setArgs[0].toLowerCase() === "p")
		 						setType = "PLAYING";
		 					else if(setArgs[0].toLowerCase() === "s")
	 							setType = "STREAMING";
	 						else if(setArgs[0].toLowerCase() === "l")
	 							setType = "LISTENING";
	 						else if(setArgs[0].toLowerCase() === "w")
	 							setType = "WATCHING";

			 				botmellow.user.setActivity(setArgs[1], {type: setType});
			 				genModule.logActivity(null, logs.setAct, args);
			 				
			 				message.channel.send(responses.setAct + args.toLowerCase() + ".");
		 				}
	 				}
	 				else {
	 					message.channel.send(responses.setActError);
	 				}
	 			break;

	 			default: 
	 				message.channel.send(responses.default);
	 			break;

		 	} // commands
		} // Direct messages
    } //
});

botmellow.on('ready',function() {
	config = genModule.loadConfig(botmellow);
	genModule.logActivity(null, logs.startUp, null);
});

botmellow.on('disconnected',function(error) {
	genModule.logError(null, null, null, error);
});

botmellow.on('idle',function(error) {
	genModule.logActivity(null, logs.idle, null);
})

// local functions
function playMusic (id, message) {
	guilds[message.guild.id].voiceChannel = message.member.voiceChannel;

	guilds[message.guild.id].voiceChannel.join().then(function (connection) {
	    stream = ytdl("https://www.youtube.com/watch?v=" + id, {
	        filter: 'audioonly'
	    });
	    guilds[message.guild.id].skipReq  = 0;
	    guilds[message.guild.id].skippers = [];

	    guilds[message.guild.id].dispatcher = connection.playStream(stream);
	    guilds[message.guild.id].dispatcher.on('end', function() {
			genModule.logActivity(message.guild.id, logs.playStop, guilds[message.guild.id].queueNames[0]);
	    	guilds[message.guild.id].skipReq  = 0;
	    	guilds[message.guild.id].skippers = [];
	    	guilds[message.guild.id].queue.shift();
	    	guilds[message.guild.id].queueNames.shift();
	    	guilds[message.guild.id].queueLengths.shift();
	    	if(guilds[message.guild.id].queue.length === 0) {
	    		guilds[message.guild.id].queue        = [];
	    		guilds[message.guild.id].queueNames   = [];
	    		guilds[message.guild.id].queueLengths = [];
	    		guilds[message.guild.id].isPlaying    = false; 
	    	}
	    	else {
	    		setTimeout(function () {
	    			message.channel.send(
	    				responses.playLine1 + 
	    				guilds[message.guild.id].queueNames[0] + 
	    				responses.playLine2 + 
	    				musicModule.convDuration(
	    					guilds[message.guild.id].queueLengths[0]
	    				)
	    			);
	    			playMusic(guilds[message.guild.id].queue[0], message);
					genModule.logActivity(message.guild.id, logs.playMusic, guilds[message.guild.id].queueNames[0]);
	    		}, 500);	
	    	}
	    });
	});
}

function skipSong(message) {
    guilds[message.guild.id].dispatcher.end();
}
